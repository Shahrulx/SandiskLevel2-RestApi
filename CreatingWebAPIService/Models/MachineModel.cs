﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestApi.Models
{
    public class UserInfo
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string UserType { get; set; }
        public string SerialNumber { get; set; }
        public string Wifi { get; set; }
        public string SelectedLines { get; set; }
    }

    public class AllMachineModel
    {
        public string MachineType { get; set; }
        public string LineList { get; set; }
    }

    public class ModelNewPM
    {
        public string LineName { get; set; }
        public string MachineName { get; set; }
        public string PmType { get; set; }
        public int Duration { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Username { get; set; }

    }

    public class ModelNewDownTime
    {
        public string Id { get; set; }
        public string PIC { get; set; }
        public int MachineId { get; set; }
        public int Duration { get; set; }
        public string CoType { get; set; }
        public string StartDate { get; set; }
        public string StationName { get; set; }
        public string LineNameMap { get; set; }
        public string DateTime { get; set; }
        public string EndTime { get; set; }
        public string Others { get; set; }
    }

    public class ModelAddCloseDowntime
    {
        public string Id { get; set; }
        public string PIC { get; set; }
        public string StationName { get; set; }
        public string LineNameMap { get; set; }
        public string StartDate { get; set; }
        public string CoType { get; set; }
        public string Others { get; set; }
    }

    public class ModelNewCO
    {
        public string LineName { get; set; }
        public string MachineName { get; set; }
        public string CoType { get; set; }
        public int Duration { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string Username { get; set; }

    }

    public class ModelAlertDone
    {
        public string AlertId { get; set; }
        public string PICName { get; set; }
        public string ActionTaken { get; set; }

    }

    public class MachineRoot
    {
        public String MachineList { get; set; }
    }

    public class ModelUpdateImageData
    {
        public string Pmid { get; set; }
        public string Filename { get; set; }
        public string FileSize { get; set; }
        public string DateAdded { get; set; }

    }
}