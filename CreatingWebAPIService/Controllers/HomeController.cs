﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace RestApi.Controllers
{
    public class HomeController : ApiController
    {

        [HttpGet]
        public string GetEmpDetails()
        {

            return "Test";

        }

        [HttpGet]
        [Route("api/GetAlert/{id}")]
        public HttpResponseMessage GetCustomerById(string id)
        {
            var Data  = SQLManager.ReturnAllAlert();
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
          
            //gets a single customer using id
        }

        [HttpGet]
        [Route("api/GetPmList/{id}")]
        public HttpResponseMessage GetPmList(string id)
        {
            var Data = SQLManager.ReturnAllAlert();
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;

            //gets a single customer using id
        }


        [HttpPost]
        [Route("api/AlertAck/{id}")]
        public string PostId(string id)
        {
            var val = id.Split(',');
            SQLManager.UpdateAlert(val[0], val[1]);

            return "AlertAck";
        }

        [HttpPost]
        [Route("api/AlertDone/{id}")]
        public string PostId2(string id)
        {
            var val = id.Split(',');
            SQLManager.UpdateDoneAlert(val[0], val[1],val[2]);

            return "AlertDone";
        }

        [HttpDelete]
        public string DeleteEmpDetails(string id)
        {
            return "Employee details deleted having Id " + id;

        }
        [HttpPut]
        public string UpdateEmpDetails(string Name)
        {
            return "Employee details Updated with Name " + Name;

        }
    }

}

