﻿using RestApi.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace RestApi.Controllers
{
    public class RestApiController : ApiController
    {
        /// <summary>
        /// REST API TRIGERRING
        /// </summary>
        /// <returns></returns>

        string imagepath = "~/Userimage/PM/";

        #region OtherRoute

        [HttpGet]
        [Route("Test")]
        public string Test()
        {
            return "Test Received";
        }


        [HttpGet]
        [Route("Version")]
        public string Version()
        {
            return "v2.5.0.1";
        }

        [HttpGet]
        [Route("CheckUpdate")]
        public string CheckUpdate()
        {
            string read = "";
            try
            {
                StreamReader reader = File.OpenText(AppDomain.CurrentDomain.BaseDirectory.ToString() + "Update/update.info");
                read = reader.ReadLine();
                reader.Close();
            }
            catch (Exception ex)
            {
                read = ex.Message;
            }

            return read;
        }

        [HttpPost]
        [Route("Reset")]
        public string Reset()
        {

            String stat = SQLManager.Reset();
            return stat;
        }

        [HttpGet]
        [Route("GetLineList")]
        public HttpResponseMessage GetLineList()
        {
            var Data = SQLManager.GetLineList();
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }

        [HttpGet]
        [Route("GetMachineList")]
        public HttpResponseMessage GetMachineList()
        {
            var Data = SQLManager.GetMachineList();
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }

        [HttpPost]
        [Route("CountAllMachine")]
        public HttpResponseMessage CountAllMachine([FromBody] AllMachineModel allMachine)
        {
            var Data = SQLManager.CountAllMachine(allMachine);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }

        #endregion


        [HttpPost]
        [Route("GetUser")]
        public HttpResponseMessage GetUser([FromBody] UserInfo userInfo)
        {
            var Data = SQLManager.GetUser(userInfo);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            return resp;
        }

        [HttpPost]
        [Route("UserLogout")]
        public HttpResponseMessage UserLogout([FromBody] UserInfo userInfo)
        {
            var Data = SQLManager.UserLogout(userInfo);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            return resp;
        }

        [HttpPost]
        [Route("SaveLine")]
        public HttpResponseMessage SaveLine([FromBody] UserInfo userInfo)
        {
            var Data = SQLManager.SaveLine(userInfo);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("text/plain");
            return resp;
        }

        [HttpPost]
        [Route("GetLine")]
        public HttpResponseMessage GetLine([FromBody] AllMachineModel allMachineModel)
        {
            var Data = SQLManager.GetLine(allMachineModel);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }

        [HttpGet]
        [Route("GetUserLine/{Userid}")]
        public HttpResponseMessage GetUserLine(string Userid)
        {
            var Data = SQLManager.GetUserLine(Userid);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
            
        }

        #region Alert Route

        [HttpGet]
        [Route("GetNewAlert/{Machines}")]
        public HttpResponseMessage GetNewAlert(string Machines)
        {
            var Data = SQLManager.GetNewAlert(Machines);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;

            //gets a single customer using id
        }


        [HttpGet]
        [Route("GetMachine/{LineId}")]
        public HttpResponseMessage GetMachine(string LineId)
        {
            var Data = SQLManager.GetMachine(LineId);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;

            //gets a single customer using id
        }

        ///to do
        //[HttpGet]
        //[Route("GetLine/{MachineType}")]
        //public HttpResponseMessage GetLine(string MachineType)
        //{
        //    var Data = SQLManager.GetLine(MachineType);
        //    var resp = new HttpResponseMessage()
        //    {
        //        Content = new StringContent(Data)
        //    };
        //    resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
        //    return resp;

        //}

        [HttpGet]
        [Route("GetLine")]
        public HttpResponseMessage GetLine()
        {
            var Data = SQLManager.GetLine();
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;

            //gets a single customer using id
        }

        [HttpPost]
        [Route("AlertAck/{id}")]
        public string AlertAck(string id)
        {
            var val = id.Split('|');
            var stat = SQLManager.UpdateAlert(val[0], val[1]);

            return stat;
        }

        [HttpPost]
        [Route("AlertDone")]
        public string AlertDone([FromBody] ModelAlertDone p)
        {
            SQLManager.UpdateDoneAlert(p.AlertId, p.PICName, p.ActionTaken);

            return "AlertDone";
        }

        [HttpGet]
        [Route("GetAlert/{MachineType}")]
        public HttpResponseMessage GetAlert(string MachineType)
        {
            var _MachineType = MachineType.Split(',');
            var Data = SQLManager.GetAlert(_MachineType[0], _MachineType[1]);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;

        }

        [HttpGet]
        [Route("GetAlertByPic/{value}")]
        public HttpResponseMessage GetAlertByPic(string value)
        {
            var val = value.Split(',');
            var Data = SQLManager.GetAlert(val[0], val[1], val[2]);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
            //gets a single customer using id
        }

        #endregion

        #region PMRoute

        [HttpGet]
        [Route("GetPmLine")]
        public HttpResponseMessage GetPmLine()
        {
            var Data = SQLManager.GetPmLine();
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }

        [HttpGet]
        [Route("GetPmMachine/{LineId}")]
        public HttpResponseMessage GetPmMachine(string LineId)
        {
            var Data = SQLManager.GetPmMachine(LineId);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }

        [HttpGet]
        [Route("GetPmMachineDetail/{PmId}")]
        public HttpResponseMessage GetPmMachineDetail(string PmId)
        {
            var Data = SQLManager.GetPmMachineDetail(PmId);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }

        [HttpGet]
        [Route("GetPmCheckList/{PmId}")]
        public HttpResponseMessage GetPmCheckList(string PmId)
        {
            var Data = SQLManager.GetPmCheckList(PmId);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }

        [HttpPost]
        [Route("UpdatePmCheckList/{id}")]
        public string UpdatePmCheckList(string id)
        {
            var val = id.Split('|');
            SQLManager.UpdatePmCheckList(val[0], val[1], val[2]);

            return "CheckListUpdated";
        }

        [HttpPost]
        [Route("AddNewPm")]
        public string AddNewPm([FromBody] ModelNewPM p)
        {
            try
            {
                SQLManager.AddPM(p.LineName, p.MachineName, p.PmType, p.Duration, p.StartDate, p.Username);
                return "Success";
            }
            catch (Exception ex)
            {
                return "Error:"+ex;
            }

        }

        [HttpPost]
        [Route("ClosePM/{data}")]
        public string ClosePM(string data)
        {
            try
            {
                var val = data.Split(',');

                var result = SQLManager.ClosePM(val[0], val[1]);
                return result;
            }
            catch (Exception ex)
            {
                return "false" + ex.Message;
            }


        }

        #endregion

        #region CoRoute
        [HttpGet]
        [Route("GetCoLine")]
        public HttpResponseMessage GetCoLine()
        {
            var Data = SQLManager.GetCoLine();
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }

        [HttpGet]
        [Route("GetCoMachine/{LineId}")]
        public HttpResponseMessage GetCoMachine(string LineId)
        {
            var Data = SQLManager.GetCoMachine(LineId);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }

        [HttpGet]
        [Route("GetCoMachineDetail/{PmId}")]
        public HttpResponseMessage GetCoMachineDetail(string PmId)
        {
            var Data = SQLManager.GetCoMachineDetail(PmId);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }

        [HttpGet]
        [Route("GetCoCheckList/{PmId}")]
        public HttpResponseMessage GetCoCheckList(string PmId)
        {
            var Data = SQLManager.GetCoCheckList(PmId);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }

        [HttpPost]
        [Route("UpdateCoCheckList/{id}")]
        public string UpdateCoCheckList(string id)
        {
            var val = id.Split('|');
            SQLManager.UpdateCoCheckList(val[0], val[1], val[2]);

            return "CheckListUpdated";
        }


        [HttpPost]
        [Route("CloseCo/{data}")]
        public string CloseCo(string data)
        {
            var val = data.Split(',');
            var ret = SQLManager.CloseCo(val[0], val[1]);
            return ret;
        }

        [HttpPost]
        [Route("AddNewCo")]
        public HttpResponseMessage AddNewCo([FromBody] ModelNewCO p)
        {
            string _Content = "";

            try
            {
                SQLManager.AddNewCo(p.LineName, p.MachineName, p.CoType, p.Duration, p.StartDate, p.Username);

                _Content = "Success";

            }
            catch (Exception ex)
            {
                _Content = "Error"+ex;

            }

            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(_Content)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;

        }


        #endregion

        #region DekRoute

        [HttpGet]
        [Route("GetAllDek")]
        public HttpResponseMessage GetAllDek()
        {
            var Data = SQLManager.GetAllDek();
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;

        }

        [HttpGet]
        [Route("GetAllDek/{id}")]
        public HttpResponseMessage GetDekById(string id)
        {
            var Data = SQLManager.GetDekById(id);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }



        #endregion

        #region DownTime Route

        [HttpGet]
        [Route("GetDownLine")]
        public HttpResponseMessage GetDownLine()
        {
            var Data = SQLManager.GetDownLine();
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }

        [HttpGet]
        [Route("GetDownMachine/{LineId}")]
        public HttpResponseMessage GetDownMachine(string LineId)
        {
            var Data = SQLManager.GetDownMachine(LineId);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }

        [HttpGet]
        [Route("GetDownMachineDetail/{PmId}")]
        public HttpResponseMessage GetDownMachineDetail(string PmId)
        {
            var Data = SQLManager.GetDownMachineDetail(PmId);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }

        // sat2 
        [HttpPost]
        [Route("AddNewDownTime")]
        public string AddNewDownTime([FromBody] ModelAddCloseDowntime p)
        {
            try
            {
                SQLManager.AddNewDownTime(p);
                return "true";
            }
            catch (Exception ex)
            {
                return "false" + ex.Message;
            }

        }

        [HttpPost]
        [Route("CloseDownTime")]
        public string CloseDownTime(ModelAddCloseDowntime m)
        {
            try
            {
                SQLManager.CloseDownTime(m);
                return "true";
            }
            catch (Exception ex)
            {
                return "false " + ex.Message;

            }
        }


        #endregion
        //  public async Task<HttpResponseMessage> PostUserImage()
        [HttpPost]
        [Route("PostUserImage/{OtherParam}")]
        public HttpResponseMessage PostUserImage(string OtherParam)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            try
            {

                var httpRequest = HttpContext.Current.Request;
                
                foreach (string file in httpRequest.Files)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);

                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {

                        int MaxContentLength = 1024 * 1024 * 1; //Size = 1 MB  

                        IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png" };
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var extension = ext.ToLower();
                        if (!AllowedFileExtensions.Contains(extension))
                        {

                            var message = string.Format("Please Upload image of type .jpg,.gif,.png.");

                            dict.Add("error", message);
                            return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        }
                        else if (postedFile.ContentLength > MaxContentLength)
                        {

                            var message = string.Format("Please Upload a file upto 1 mb.");

                            dict.Add("error", message);
                            return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        }
                        else
                        {
                            // var filePath = HttpContext.Current.Server.MapPath(imagepath + postedFile.FileName + extension);
                            var filePath = HttpContext.Current.Server.MapPath(imagepath + postedFile.FileName);
                            postedFile.SaveAs(filePath);
                            var Data = SQLManager.UpdateImageData(postedFile.FileName, postedFile.ContentLength.ToString(), OtherParam:OtherParam);
                           
                        }
                    }

                    var message1 = string.Format("Image Updated Successfully.");
                    return Request.CreateErrorResponse(HttpStatusCode.Created, message1); ;
                }
                var res = string.Format("Please Upload a image.");
                dict.Add("error", res);
                return Request.CreateResponse(HttpStatusCode.NotFound, dict);
            }
            catch (Exception ex)
            {
                var res = string.Format("some Message" + ex.Message);
                dict.Add("error", res);
                return Request.CreateResponse(HttpStatusCode.NotFound, dict);
            }

        }

        


        //demo

        [HttpGet]
        [Route("CheckAnswer/{TicketId}")]
        public HttpResponseMessage CheckAnswer(string TicketId)
        {
            var Data = SQLManager.CheckAnswer(TicketId);
            var resp = new HttpResponseMessage()
            {
                Content = new StringContent(Data)
            };
            resp.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return resp;
        }

    }

}
