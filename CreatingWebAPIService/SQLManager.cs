﻿using Newtonsoft.Json;
using RestApi.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Configuration;

namespace RestApi
{
    public class SQLManager
    {
        static string ServerName = WebConfigurationManager.AppSettings["ServerName"];
        static string Database = WebConfigurationManager.AppSettings["Database"];
        static string UserName = WebConfigurationManager.AppSettings["UserName"];
        static string Password = WebConfigurationManager.AppSettings["Password"];
        static string DebugMode = WebConfigurationManager.AppSettings["debug"];

        private static string connectionString = $@"Data Source={ServerName};User Id={UserName};Password={Password};database={Database};MultipleActiveResultSets=True;";

        //private static string connectionString = @"Data Source=sdmsqlid10;User Id=ssdme_yield;Password=MeYield1234;database=SSDENG01;";
        //private static string connectionString = @"Data Source=192.168.1.84;User Id=izzati;Password=izzati;database=sandisk;";

        //private static string connectionString = Properties.Settings.Default.MSSQLSource.ToString();

        //static SqlConnection connection = new SqlConnection(connectionString);
        private static readonly Object connectionLock = new Object();


        private static readonly Object DataReadLock = new Object();
        private static readonly Object GetADataLock = new Object();

        public static bool isDomainName = false;
        static DataTable dt;
        //static SqlCommand Command;
        static SqlDataReader dr;

        public static string Test()
        {
            SqlConnection conn = new SqlConnection(connectionString);

            string sql = string.Empty;
            sql = string.Format("select * from View1 where Status='NEW' and (onoff=1 or onoff is NULL) ");
            DataTable val = GetDataToDatatable(conn, sql);

            return sql;

        }

        public static string GetUser(UserInfo userInfo)
        {
            SqlConnection conn = new SqlConnection(connectionString);

            string sql = string.Empty;
            Password = Encrypt(userInfo.Password);
            //sql = string.Format($"select * from Users where Username='{UserName}' and Password='{Password}' ");
            sql = string.Format($"select * from Users where Username='{userInfo.Username}' and Password='{Password}' ");
            DataTable val = GetDataToDatatable(conn, sql);

            try
            {
                if (val.Rows.Count > 0)
                {
                    string DeviceId = "0";

                    sql = string.Format($"select id from Devices where Wifi='{userInfo.Wifi}' ");
                    DataTable val2 = GetDataToDatatable(conn, sql);
                    WriteToLog(sql);
                    if (val2.Rows.Count > 0)
                    {
                        DeviceId = val2.Rows[0]["id"].ToString();
                    }
                    else
                    {
                        sql = $"Insert into Devices(Wifi,DeviceType) values ('{userInfo.Wifi}','Wearable')";
                        WriteToLog(sql);
                        AddOrUpdateToDatabase(conn, sql);
                        sql = string.Format($"select id from Devices where Wifi='{userInfo.Wifi}' ");
                        DeviceId = GetAData(conn, sql, "id");
                    }

                    var id = val.Rows[0]["Id"].ToString();
                    userInfo.UserType = val.Rows[0]["UserType"].ToString();
                    userInfo.SelectedLines = "";

                    //sql = string.Format($"select a.*, b.DeviceName from AccessHistory a  left join Devices b on b.id = a.Deviceid  where a.UserId = {id} and b.Wifi = '{userInfo.Wifi}' and a.LogoutTime is null");
                    sql = $"select a.*,b.Wifi from AccessHistory a  left join Devices b on b.id = a.Deviceid  where a.UserId ={id}  and b.DeviceType='Wearable' and a.LogoutTime is null";
                    WriteToLog(sql);
                    DataTable dt2 = GetDataToDatatable(conn, sql);

                    var AccessID = "";

                    if (dt2.Rows.Count == 0)
                    {
                        //no logout everyine can log in
                        sql = $"select Id from AccessHistory where DeviceId ={DeviceId} and LogoutTime is null";
                        var dtTemp = GetDataToDatatable(conn, sql);
                        foreach (DataRow row in dtTemp.Rows)
                        {
                            var _deviceId= row["Id"].ToString();
                            sql = $"update AccessHistory set LogoutTime=GetDate() where id={_deviceId}";
                            AddOrUpdateToDatabase(conn, sql);
                        }
                            
                        

                        sql = $"insert into AccessHistory(UserId,DeviceId,LoginTime) values ({id},'{DeviceId}',GETDATE())";
                        WriteToLog(sql);
                        AddOrUpdateToDatabase(conn, sql);

                    }
                    else
                    {
                        //already got user get device mac
                        var Mac = dt2.Rows[0]["Wifi"].ToString();
                        DateTime LastResponse = DateTime.Parse(dt2.Rows[0]["LastResponse"].ToString());
                        WriteToLog(Mac);
                        var dateTimeNow = DateTime.Now;

                        var getTimeDiff = dateTimeNow - LastResponse;
                        var _timediff = getTimeDiff.TotalMinutes;

                        //too long no responce


                        if (Mac == userInfo.Wifi)
                        {
                            //same device logout last login
                            AccessID = dt2.Rows[0]["Id"].ToString();
                            if (AccessID != "" || AccessID != null)
                            {
                                sql = $"select Id from AccessHistory where DeviceId ={DeviceId} and LogoutTime is null";
                                var dtTemp = GetDataToDatatable(conn, sql);
                                foreach (DataRow row in dtTemp.Rows)
                                {
                                    var _AccessId = row["Id"].ToString();
                                    sql = $"update AccessHistory set LogoutTime=GetDate() where id={_AccessId}";
                                    AddOrUpdateToDatabase(conn, sql);
                                }

                                sql = $"update AccessHistory set LogoutTime=GETDATE() where id='{AccessID}' ";
                                WriteToLog(sql);
                                AddOrUpdateToDatabase(conn, sql);
                            }

                            sql = $"insert into AccessHistory(UserId,DeviceId,LoginTime) values ({id},'{DeviceId}',GETDATE())";
                            AddOrUpdateToDatabase(conn, sql);
                        }
                        else
                        {
                            //same user diffrent device cannot login
                            userInfo.UserType = "LOCK";
                        }


                    }



                    string output = JsonConvert.SerializeObject(userInfo);
                    return output;
                }

                else
                {
                    userInfo.UserType = "null";
                    WriteToLog("HERE");
                    string output = JsonConvert.SerializeObject(userInfo);
                    return output;
                }

            }
            catch (Exception ex)
            {

                return ex.Message;
            }




        }

        public static string UserLogout(UserInfo userInfo)
        {
            SqlConnection conn = new SqlConnection(connectionString);

            string sql = string.Empty;
            Password = Encrypt(Password);
            sql = string.Format($"select Id from Users where Username='{userInfo.Username}'");
            var _id = GetAData(conn, sql, "Id");


            sql = string.Format($"select top 1 id from AccessHistory where Userid={_id} order by id desc");
            _id = GetAData(conn, sql, "Id");

            sql = $"update AccessHistory set LogoutTime=GetDate() where id={_id}";
            WriteToLog(sql);
            try
            {
                DataTable val = GetDataToDatatable(conn, sql);
                return "1";
            }
            catch (Exception)
            {

                return "0";
            }


        }

        public static string GetNewAlert(string MachineList)
        {
            SqlConnection conn = new SqlConnection(connectionString);

            string sql = "SELECT MachineType,count(id) as ErrorCount  FROM [sandisk].[dbo].[View1] where Status in ('NEW','ACK') and (onoff=1  or onoff is null) group by MachineType";

            //MachineList = "AOI,CPK,DEK,NXT,CPK";
            //var mlist = MachineList.Split(',');
            //string sql = "SELECT";

            //foreach (var item in mlist)
            //{
            //    sql += $"(SELECT count(id)  FROM [sandisk].[dbo].[View1] where Status in ('NEW','ACK') and MachineType='{item}' and (onoff=1 or onoff is null)) AS {item},";
            //}

            //sql = sql.Remove(sql.Count() - 1);
            return DataTableToJSONWithJSONNet(GetDataToDatatable(conn, sql));
        }

        public static string ReturnAllAlert()
        {
            SqlConnection conn = new SqlConnection(connectionString);
            string sql = string.Empty;
            sql = string.Format("select * from View1 where Status='NEW' and (onoff=1 or onoff is NULL)  ");
            //sql = string.Format("select * from View2 ");
            return DataTableToJSONWithJSONNet(GetDataToDatatable(conn, sql));

        }

        public static string GetLineList()
        {
            SqlConnection conn = new SqlConnection(connectionString);

            DataTable tbl = new DataTable();

            DataColumn workCol = tbl.Columns.Add("LineName", typeof(String));

            workCol = tbl.Columns.Add("PicCount", typeof(String));

            Dictionary<string, int> LineCount = new Dictionary<string, int>();

            string sql = "select  * from (select UserId, DeviceId, LoginTime, LogoutTime, LastResponse,AccessId, LineSelected, " + "" +
                "DateAdded, FullName, USername, UserType, ROW_NUMBER() OVER(PARTITION BY USerid ORDER BY LoginTime DESC) rn FROM    AccessHistory left join " +
                "LineHistory on AccessHistory.Id = LineHistory.AccessId left join Users on AccessHistory.UserId = Users.Id where AccessHistory.LogoutTime is NULL) a " +
                "WHERE   a.rn = 1 order by UserId desc";

            WriteToLog(sql);

            var dt1 = GetDataToDatatable(conn, sql);

            int i = 0;

            try
            {
                foreach (DataRow row in dt1.Rows)
                {
                    string lineName = row["LineSelected"].ToString();

                    if (lineName != null || lineName =="")
                    {
                        var _lineName = lineName.Split(',');
                        foreach (var item in _lineName)
                        {
                            var check = LineCount.ContainsKey(item);
                            if (!check)
                            {
                                LineCount.Add(item, 1);
                            }
                            else
                            {
                                var getCurrentCount = LineCount[item];
                            }
                        }
                    }
                }
                sql = "select LineNameMap as linename from Lines";
                var dt2 = GetDataToDatatable(conn, sql);

                foreach (DataRow row2 in dt2.Rows)
                {
                    string linename = row2["linename"].ToString();

                    var check2 = LineCount.ContainsKey(linename);
                    if (check2)
                    {
                        tbl.Rows.Add(linename, LineCount[linename]);
                    }
                    else
                    {
                        tbl.Rows.Add(linename, 0);
                    }
                }


                return DataTableToJSONWithJSONNet(tbl);
            }
            catch (Exception ex)
            {

                return ex.Message;
            }





        }


        public static string GetMachineList()
        {
            SqlConnection conn = new SqlConnection(connectionString);

            string sql = "select DISTINCT MachineType from ErrorCode";

            return DataTableToJSONWithJSONNet(GetDataToDatatable(conn, sql));
        }

        public static string GetMachine(string LineId)
        {
            SqlConnection conn = new SqlConnection(connectionString);

            LineId = LineId.Replace("_", ".");
            string sql = string.Empty;
            if (LineId != "")
            {
                sql = string.Format("select Id from Lines where LineNameMap='{0}'", LineId);
                var line = GetAData(conn, sql, "Id");
                sql = string.Format("select StationName from Stations where LineId={0}", line);
            }

            else
                sql = string.Format("select StationName from Stations  ");

            return DataTableToJSONWithJSONNet(GetDataToDatatable(conn, sql));

        }

        public static string GetLine(string machine = "")
        {
            SqlConnection conn = new SqlConnection(connectionString);
            DataTable LineList = new DataTable();
            string sql = string.Empty;

            sql = string.Format("select LineNameMap from Lines");
            LineList = GetDataToDatatable(conn, sql);

            return DataTableToJSONWithJSONNet(LineList);
        }

        public static string GetLine(AllMachineModel allMachine)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            DataTable LineList = new DataTable();
            string sql = string.Empty;

            string Lines = "";

            var _linelist = allMachine.LineList.Split(',');
            foreach (var item in _linelist)
            {
                Lines = Lines + "'" + item + "',";
            }

            Lines = Lines.Remove(Lines.Length - 1);

            if (allMachine.MachineType == "")
            {
                sql = string.Format("select LineNameMap from Lines");
                LineList = GetDataToDatatable(conn, sql);
            }
            else if (allMachine.MachineType == "CPK")
            {
                sql = "SELECT  Distinct EquipmentId  FROM CPK order by EquipmentId ";
                var dt1 = GetDataToDatatable(conn, sql);
                string ErrorCode = "";

                foreach (DataRow row in dt1.Rows)
                {
                    string Id = row["EquipmentId"].ToString();
                    sql = $"SELECT count(EquipmentId) as ErrorCount FROM CPK where Status in ('NEW','ACK') and EquipmentId='" + Id + "' ";

                    var tb1 = GetDataToDatatable(conn, sql);
                    var ecount = tb1.Rows[0]["ErrorCount"].ToString();

                    if (ecount != "0")
                    {
                        DataColumn workCol = tb1.Columns.Add("LineName", typeof(String));
                        tb1.Columns.Add("LineId", typeof(String));
                        tb1.Columns.Add("OtherParam", typeof(String));
                        tb1.Rows[0]["LineId"] = Id;
                        tb1.Rows[0]["LineName"] = Id;
                        tb1.Rows[0]["OtherParam"] = "None";

                        LineList.Merge(tb1);
                    }
                }
            }
            else
            {
                sql = $"select LineName as LineName,COUNT(id) as ErrorCount  from View1 where MachineType='{allMachine.MachineType}' and Status in('NEW','ACK') and LineName in ({Lines}) group by LineName";
                LineList = GetDataToDatatable(conn, sql);

            }

            return DataTableToJSONWithJSONNet(LineList);

        }

        public static string GetUserLine(string Username)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            DataTable LineList = new DataTable();


            string sql = string.Format($"select Id from Users where Username='{Username}'");
            var _id = GetAData(conn, sql, "Id");

            sql = string.Format($"Select top 1 LineSelected from LineHistory left join AccessHistory on LineHistory.AccessId = AccessHistory.Id where UserId='{_id}' order by DateAdded desc");
            var LineSelected = GetAData(conn, sql, "LineSelected");

            return LineSelected;
        }


        public static string SaveLine(UserInfo userInfo)
        {
            SqlConnection conn = new SqlConnection(connectionString);

            try
            {

                string sql = string.Format($"select Id from Users where Username='{userInfo.Username}'");
                var _id = GetAData(conn, sql, "Id");

                sql = $"insert into LineHistory (AccessId, LineSelected, DateAdded) OUTPUT INSERTED.Id values ((select top 1 Id from AccessHistory where UserId='" + _id + "' order by LoginTime desc), '" + userInfo.SelectedLines + "', CURRENT_TIMESTAMP)";
                string insertedId = AddAndReturn(conn, sql);

                sql = "insert into AuditTrail (UserId, TableChanges, Changes, DateChanges) values ('" + _id + "', 'LineHistory', 'Add " + insertedId + "', CURRENT_TIMESTAMP)";
                AddOrUpdateToDatabase(conn, sql);


                return "1";
            }
            catch (Exception)
            {

                return "0";
            }


        }

        #region PM
        internal static string GetPmLine()
        {
            SqlConnection conn = new SqlConnection(connectionString);
            string sql = string.Empty;
            string time = DateTime.Today.ToString("yyyy-MM-dd");

            sql = " SELECT count(PreventiveMaintenance.LineId ) as c,Lines.LineNameMap ,Lines.Id  FROM PreventiveMaintenance";
            sql += " left join Lines on Lines.Id = PreventiveMaintenance.LineId";
            sql += " where  Lines.LineNameMap is not null and PreventiveMaintenance.EndTime is null and StartDate<'" + time + " 23:59:59'";
            sql += " group by Lines.LineNameMap , Lines.Id ";

            var dt = GetDataToDatatable(conn, sql);
            return DataTableToJSONWithJSONNet(dt);
        }

        public static string GetPmMachine(string LineId)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            string t = DateTime.Today.ToString("yyyy-MM-dd");
            //string t = "2017-10-21";

            string sql = " SELECT PreventiveMaintenance.Id, PreventiveMaintenance.PIC, PreventiveMaintenance.MachineId, PreventiveMaintenance.Duration, PreventiveMaintenance.PmType, Stations.StationName, Lines.LineNameMap";
            sql += "  ,PreventiveMaintenance.DateTime, PreventiveMaintenance.EndTime";
            sql += " FROM PreventiveMaintenance INNER JOIN";
            sql += " Stations ON PreventiveMaintenance.MachineId = Stations.Id INNER JOIN";
            sql += " Lines ON PreventiveMaintenance.LineId = Lines.Id Where  PreventiveMaintenance.EndTime IS NULL  and StartDate<'" + t + " 23:59:59' and  PreventiveMaintenance.LineId='" + LineId + "' Order by PreventiveMaintenance.DateTime asc";

            var dt = GetDataToDatatable(conn, sql);
            return DataTableToJSONWithJSONNet(dt);

        }

        public static string GetPmMachineDetail(string PmId)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            string t = DateTime.Today.ToString("yyyy-MM-dd");
            string sql = " SELECT  PreventiveMaintenance.Id,PreventiveMaintenance.StartDate,  PreventiveMaintenance.PIC, PreventiveMaintenance.MachineId, PreventiveMaintenance.Duration, PreventiveMaintenance.PmType, Stations.StationNameMap, Lines.LineNameMap";
            sql += "  ,PreventiveMaintenance.DateTime";
            sql += " FROM PreventiveMaintenance INNER JOIN";
            sql += " Stations ON PreventiveMaintenance.MachineId = Stations.Id INNER JOIN";
            sql += " Lines ON PreventiveMaintenance.LineId = Lines.Id Where PreventiveMaintenance.EndTime IS NULL and StartDate<'" + t + " 23:59:59' AND PreventiveMaintenance.Id='" + PmId + "' Order by PreventiveMaintenance.DateTime desc ";

            var dt = GetDataToDatatable(conn, sql);
            return DataTableToJSONWithJSONNet(dt);
        }


        public static string GetPmCheckList(string Pmid)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            string sql = "";
            sql = $"SELECT PMChecklist.*,Checklist.Checklist from PMChecklist LEFT JOIN Checklist ON PMChecklist.ChecklistId = Checklist.Id where PMChecklist.PmId ='{Pmid}'";
            //sql = "  select * from PMChecklist where PmId='" + Pmid + "' ";
            var dt = GetDataToDatatable(conn, sql);
            var stat = dt.Rows.Count;
            if (stat == 0)
                return "0";
            else
                return DataTableToJSONWithJSONNet(dt);
        }

        public static void UpdatePmCheckList(string Pmid, String PIC, string ON)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            string sql = "";

            sql = string.Format($"select Id from Users where Username='{PIC}'");
            var UserID = GetAData(conn, sql, "Id");

            if (ON == "True")
                sql = $"Update PMChecklist Set PIC={UserID},EndTime=getdate() where Id='" + Pmid + "'";
            else
                sql = $"Update PMChecklist Set PIC=NULL,EndTime=NULL where Id='" + Pmid + "'";

            AddOrUpdateToDatabase(conn, sql);


        }


        public static string ClosePM(string Pmid, String userId)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            string sql = "";

            sql = $"select count(id) as c from [sandisk].[dbo].[PMChecklist] where PmId={Pmid} and EndTime is null";
            var count = GetAData(conn, sql, "c");

            if (count != "0")
            {
                return "false";
            }

            sql = " Update PreventiveMaintenance Set EndTime=getdate() where Id='" + Pmid + "'";
            AddOrUpdateToDatabase(conn, sql);

            sql = "insert into AuditTrail (UserId, TableChanges, Changes) values ('" + userId + "','PreventiveMaintenance','Edit " + Pmid + "')";
            AddOrUpdateToDatabase(conn, sql);

            return "true";

        }

        public static string UpdateBeacon(string positions)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            string sql = "";

            try
            {
                var _positions = positions.Split(',');
                sql = $"  insert into Beacon(xpos,ypos,startdate) values ('{_positions[0]}','{_positions[1]}',getdate())";
                AddOrUpdateToDatabase(conn, sql);
                return "true";
            }
            catch (Exception ex)
            {

                return "false:" + ex.Message;
            }



        }

        public static string CheckAnswer(string ticketId)
        {

            SqlConnection conn = new SqlConnection(connectionString);

            var result = "false";
            string sql = string.Empty;
            //sql = string.Format($"select * from Users where Username='{UserName}' and Password='{Password}' ");
            sql = string.Format($"select Flag from Data where id={ticketId} ");


            try
            {
                DataTable val = GetDataToDatatable(conn, sql);
                if (val.Rows.Count > 0)
                {
                    var Flag = val.Rows[0]["Flag"].ToString();
                    if (Flag.ToLower() == "1" || Flag.ToLower() == "true")
                    {
                        result = "true";
                    }
                    else
                    {
                        result = "false";
                    }
                }
            }
            catch (Exception ex)
            {
                return "error:" + ex.Message;
            }

            return result;
        }

        public static string UpdateImageData(string filename, string FileSize, string OtherParam)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            string sql = "";

            var data = OtherParam.Split(',');

            if (data[0].ToLower().Contains("pm"))
            {
                sql = "insert into PreventiveMaintenanceUpload (PMId,FileName,FileSize,DateAdded)" +
                $"values({data[1]},'{filename}','{FileSize}','{DateTime.Now}')";
                AddOrUpdateToDatabase(conn, sql);
            }
            else
            {

            }



            return "true";
        }

        internal static void AddPM(string lineName, string machineName, string pmType, int duration, string startDate, string username)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            lineName = lineName.Replace("_", ".");

            WriteToLog(lineName + "," + machineName + "," + pmType + "," + duration + "," + startDate + "," + username);

            string sql = string.Empty;
            sql = string.Format($"select Id from Users where Username='{username}'");
            var UserID = GetAData(conn, sql, "Id");

            sql = string.Format($"select Id from Lines where LineNameMap='{lineName}'");
            var _LineId = GetAData(conn, sql, "Id");

            sql = string.Format($"select Id from Stations where StationName='{machineName}'");
            var _MachineId = GetAData(conn, sql, "Id");


            var LineId = int.Parse(_LineId);
            var MachineId = int.Parse(_MachineId);
            var Duration = duration;
            var _PIC = int.Parse(UserID);

            var _datetime = startDate.Split(' ');


            var _date = _datetime[0].Split('-');
            var _date2 = _date[2] + "-" + _date[0] + "-" + _date[1];
            var _time = _datetime[1].Replace('-', ':');
            _date2 = _date2 + " " + _time;



            sql = "";
            sql += "  INSERT INTO PreventiveMaintenance (LineId,MachineId,PmType,Duration,PIC,StartDate) VALUES ";
            sql += "   (" + LineId + "";
            sql += "  , " + MachineId + "";
            sql += "  , '" + pmType + "'";
            sql += "  , " + Duration + "";
            sql += " ,'" + _PIC + "'";
            sql += " ,'" + _date2 + "')";

            WriteToLog(sql);

            AddOrUpdateToDatabase(conn, sql);
        }

        #endregion

        #region CO

        internal static string GetCoLine()
        {
            SqlConnection conn = new SqlConnection(connectionString);
            string sql = string.Empty;
            string time = DateTime.Today.ToString("yyyy-MM-dd");

            sql = " SELECT count(ChangeOver.LineId ) as c,Lines.LineNameMap ,Lines.Id  FROM ChangeOver";
            sql += " left join Lines on Lines.Id = ChangeOver.LineId";
            sql += " where ChangeOver.Cotype!='DOWNTIME' and ChangeOver.EndTime is null  and StartDate<'" + time + " 23:59:59'";
            sql += " group by Lines.LineNameMap , Lines.Id ";

            var dt = GetDataToDatatable(conn, sql);
            return DataTableToJSONWithJSONNet(dt);
        }

        public static string GetCoMachine(string LineId)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            string t = DateTime.Today.ToString("yyyy-MM-dd");
            //string t = "2017-10-21";

            string sql = " SELECT ChangeOver.Id, ChangeOver.PIC, ChangeOver.MachineId, ChangeOver.Duration, ChangeOver.COType, Stations.StationName, Lines.LineNameMap";
            sql += "  ,ChangeOver.DateTime, ChangeOver.EndTime";
            sql += " FROM ChangeOver INNER JOIN";
            sql += " Stations ON ChangeOver.MachineId = Stations.Id INNER JOIN";
            sql += " Lines ON ChangeOver.LineId = Lines.Id Where ChangeOver.Cotype!='DOWNTIME' and ChangeOver.EndTime IS  NULL and StartDate<'" + t + " 23:59:59' and  ChangeOver.LineId='" + LineId + "' Order by ChangeOver.DateTime asc";

            var dt = GetDataToDatatable(conn, sql);
            return DataTableToJSONWithJSONNet(dt);

        }

        public static string GetCoMachineDetail(string PmId)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            string t = DateTime.Today.ToString("yyyy-MM-dd");
            string sql = " SELECT  ChangeOver.Id, ChangeOver.PIC, ChangeOver.MachineId, ChangeOver.Duration, ChangeOver.COType, ChangeOver.StartDate, Stations.StationNameMap, Lines.LineNameMap";
            sql += "  ,ChangeOver.DateTime, ChangeOver.EndTime";
            sql += " FROM ChangeOver INNER JOIN";
            sql += " Stations ON ChangeOver.MachineId = Stations.Id INNER JOIN";
            sql += " Lines ON ChangeOver.LineId = Lines.Id Where ChangeOver.EndTime IS NULL and StartDate<'" + t + " 23:59:59' AND ChangeOver.Id='" + PmId + "' Order by ChangeOver.DateTime desc ";

            var dt = GetDataToDatatable(conn, sql);
            return DataTableToJSONWithJSONNet(dt);
        }

        public static string GetCoCheckList(string Pmid)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            string sql = "";
            sql = "  select * from COChecklist where CoId='" + Pmid + "' ";
            var dt = GetDataToDatatable(conn, sql);
            var stat = dt.Rows.Count;
            if (stat == 0)
                return "0";
            else
                return DataTableToJSONWithJSONNet(dt);
        }

        public static void UpdateCoCheckList(string Pmid, String PIC, string ON)
        {
            string sql = "";
            SqlConnection conn = new SqlConnection(connectionString);
            sql = string.Format($"select Id from Users where Username='{PIC}'");
            var UserID = GetAData(conn, sql, "Id");

            if (ON == "True")
                sql = $"Update COChecklist Set PIC={UserID},EndTime=getdate() where Id='" + Pmid + "'";
            else
                sql = $"Update COChecklist Set PIC=NULL,EndTime=NULL where Id='" + Pmid + "'";

            AddOrUpdateToDatabase(conn, sql);


        }

        public static string CloseCo(string Pmid, String userId)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            string sql = "";

            sql = $"select count(id) as c from [sandisk].[dbo].[COChecklist] where CoId={Pmid} and EndTime is null";
            var count = GetAData(conn, sql, "c");

            if (count != "0")
            {
                return "false";
            }

            sql = " Update ChangeOver Set EndTime=getdate() where Id='" + Pmid + "'";
            AddOrUpdateToDatabase(conn, sql);
            return "true";
        }

        internal static void AddNewCo(string lineName, string machineName, string coType, int duration, string startDate, string username)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            string sql = string.Empty;

            WriteToLog(lineName + "," + machineName + "," + coType + "," + duration + "," + startDate + "," + username);

            sql = string.Format($"select Id from Users where Username='{username}'");
            var UserID = GetAData(conn, sql, "Id");

            if (lineName.Contains('_'))
            {
                lineName = lineName.Replace("_", ".");
            }

            //var _lineName = lineName.Split('_');
            //lineName = _lineName[0] + "." + _lineName[1];
            //sql = string.Format($"select Id from Lines where LineNameMap='{lineName}'");
            //var _LineId = GetAData(conn, sql, "Id");

            //sql = string.Format($"select Id from Stations where StationName='{machineName}'");
            //var _MachineId = GetAData(conn, sql, "Id");

            sql = string.Format($"select Id from Lines where LineNameMap='{lineName}'");
            var _LineId = GetAData(conn, sql, "Id");
            WriteToLog(sql);

            sql = string.Format($"select Id from Stations where StationName='{machineName}'");
            var _MachineId = GetAData(conn, sql, "Id");

            var LineId = int.Parse(_LineId);
            var MachineId = int.Parse(_MachineId);
            var Duration = duration;
            var _PIC = int.Parse(UserID);

            var _datetime = startDate.Split(' ');


            var _date = _datetime[0].Split('-');
            var _date2 = _date[2] + "-" + _date[0] + "-" + _date[1];
            var _time = _datetime[1].Replace('-', ':');
            _date2 = _date2 + " " + _time;

            sql = "";
            sql += "  INSERT INTO ChangeOver (LineId,MachineId,COType,Duration,PIC,StartDate)  OUTPUT INSERTED.Id VALUES ";
            sql += "   (" + LineId + "";
            sql += "  , " + MachineId + "";
            sql += "  , '" + coType + "'";
            sql += "  , " + Duration + "";
            sql += " ,'" + _PIC + "'";
            sql += " ,'" + _date2 + "')";

            WriteToLog(sql);
            AddOrUpdateToDatabase(conn, sql);
        }


        #endregion

        #region DOwntime

        internal static string GetDownLine()
        {
            SqlConnection conn = new SqlConnection(connectionString);
            string sql = string.Empty;
            string time = DateTime.Today.ToString("yyyy-MM-dd");

            sql = " SELECT count(ChangeOver.LineId ) as c,Lines.LineNameMap ,Lines.Id  FROM ChangeOver ";
            sql += " left join Lines on Lines.Id = ChangeOver.LineId";
            sql += " where ChangeOver.Cotype='DOWNTIME' and ChangeOver.EndTime is null  and StartDate<'" + time + " 23:59:59'";
            sql += " group by Lines.LineNameMap , Lines.Id ";

            var dt = GetDataToDatatable(conn, sql);
            return DataTableToJSONWithJSONNet(dt);
        }

        public static string GetDownMachine(string LineId)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            string t = DateTime.Today.ToString("yyyy-MM-dd");
            //string t = "2017-10-21";

            string sql = " SELECT ChangeOver.Id, ChangeOver.PIC, ChangeOver.MachineId, ChangeOver.Duration, ChangeOver.COType, Stations.StationName, Lines.LineNameMap";
            sql += "  ,ChangeOver.DateTime, ChangeOver.EndTime,ChangeOver.Issue";
            sql += " FROM ChangeOver INNER JOIN ";
            sql += " Stations ON ChangeOver.MachineId = Stations.Id INNER JOIN";
            sql += " Lines ON ChangeOver.LineId = Lines.Id Where ChangeOver.Cotype='DOWNTIME' and ChangeOver.EndTime IS  NULL and StartDate<'" + t + " 23:59:59' and  ChangeOver.LineId='" + LineId + "' Order by ChangeOver.DateTime asc";

            var dt = GetDataToDatatable(conn, sql);
            return DataTableToJSONWithJSONNet(dt);

        }

        public static string GetDownMachineDetail(string PmId)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            string t = DateTime.Today.ToString("yyyy-MM-dd");
            string sql = " SELECT  ChangeOver.Id, ChangeOver.PIC, ChangeOver.MachineId, ChangeOver.Duration, ChangeOver.COType, ChangeOver.StartDate, Stations.StationName, Lines.LineNameMap";
            sql += "  ,ChangeOver.DateTime, ChangeOver.EndTime,COChecklist.Others";
            sql += " FROM ChangeOver ";
            sql += " RIGHT JOIN COChecklist ON COChecklist.CoId =  ChangeOver.Id";
            sql += " INNER JOIN Stations ON ChangeOver.MachineId = Stations.Id ";
            sql += " INNER JOIN Lines ON ChangeOver.LineId = Lines.Id Where ChangeOver.Cotype='DOWNTIME' and ChangeOver.EndTime IS NULL and StartDate<'" + t + " 23:59:59' AND ChangeOver.LineId='" + PmId + "' Order by ChangeOver.DateTime desc ";

            var dt = GetDataToDatatable(conn, sql);
            return DataTableToJSONWithJSONNet(dt);
        }




        public static void CloseDownTime(ModelAddCloseDowntime m)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            string sql = "";
            sql = $" Update ChangeOver Set EndTime=getdate() where Id='" + m.Id + "'";
            AddOrUpdateToDatabase(conn, sql);
            WriteToLog("UpdateQuery:" + sql);
            sql = $"update COChecklist set EndTime=getDate() where CoId={m.Id}";
            AddOrUpdateToDatabase(conn, sql);
            WriteToLog("UpdateQuery:" + sql);


            sql = string.Format($"select Id from Users where Username='{m.PIC}'");
            var UserID = GetAData(conn, sql, "Id"); // PIC kat sini mmg dah dlm bentuk Id, so yeah skip hat ni
            WriteToLog("SelectQuery:" + sql);
            WriteToLog("UserId:" + UserID);

            var lineName = m.LineNameMap;

            if (m.LineNameMap.Contains('_'))
            {
                var _lineName = m.LineNameMap.Split('_');
                lineName = _lineName[0] + "." + _lineName[1];
            }

            sql = string.Format($"select Id from Lines where LineNameMap='{lineName}'");
            var _LineId = GetAData(conn, sql, "Id");

            sql = string.Format($"select Id from Stations where StationName='{m.StationName}'");
            var _MachineId = GetAData(conn, sql, "Id");

            var LineId = int.Parse(_LineId);
            var MachineId = int.Parse(_MachineId);

            //sql = $"INSERT INTO COChecklist (CoId,MachineId,Others,PIC,EndTime) VALUES ({m.Id},{MachineId},'{m.Others}',{UserID},getDate())";
            sql = $"INSERT INTO COChecklist (CoId,MachineId,Others,PIC,EndTime) VALUES ({m.Id},{MachineId},'{m.Others}',{UserID},getDate())";
            AddOrUpdateToDatabase(conn, sql);
            WriteToLog("InsertQuery:" + sql);
        }

        internal static void AddNewDownTime(ModelAddCloseDowntime m)
        {
            SqlConnection conn = new SqlConnection(connectionString);


            string sql = string.Empty;
            sql = string.Format($"select Id from Users where Username='{m.PIC}'");
            var UserID = GetAData(conn, sql, "Id");
            WriteToLog("SelectQuery:" + sql);
            WriteToLog("UserId:" + UserID);
            var lineName = m.LineNameMap;

            if (m.LineNameMap.Contains('_'))
            {
                var _lineName = m.LineNameMap.Split('_');
                lineName = _lineName[0] + "." + _lineName[1];
            }

            sql = string.Format($"select Id from Lines where LineNameMap='{lineName}'");
            var _LineId = GetAData(conn, sql, "Id");

            sql = string.Format($"select Id from Stations where StationName='{m.StationName}'");
            var _MachineId = GetAData(conn, sql, "Id");

            var date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            var LineId = int.Parse(_LineId);
            var MachineId = int.Parse(_MachineId);


            var _datetime = m.StartDate.Split(' ');
            var _date = _datetime[0];
            var _time = _datetime[1].Replace('-', ':');
            _date = _date + " " + _time;



            sql = "";
            sql += "  INSERT INTO ChangeOver (LineId,MachineId,CoType,PIC,StartDate) OUTPUT INSERTED.Id  VALUES ";
            sql += "   (" + LineId + "";
            sql += "  , " + MachineId + "";
            sql += "  , '" + m.CoType + "'";
            sql += " ,'" + UserID + "'";
            sql += " ,'" + _date + "')";

            string result = AddAndReturn(conn, sql);
            WriteToLog("InsertQuery:" + sql);
            sql = "";
            sql = $"INSERT INTO COChecklist (CoId,MachineId,Others,PIC) VALUES ({result},{MachineId},'{m.Others}',{UserID})";


            AddOrUpdateToDatabase(conn, sql);
            WriteToLog("InsertQuery:" + sql);
        }


        #endregion

        #region GetAlertRoute
        public static string GetAlert(string MachineType, string LineId)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            string sql = string.Empty;

            if (LineId.Contains("_"))
            {
                LineId = LineId.Replace("_", ".");
            }

            if (MachineType == "CPK")
            {
                sql = string.Format($"select *,Cpk as ErrorCode from CPK where MachineType='CPK'  and Status='NEW'  and  EquipmentId='{LineId}'  ");

            }
            else
            {
                sql = string.Format($"select * from View1 where MachineType='{MachineType}' and LineName='{LineId}' and Status='NEW' and (onoff=1 or onoff is NULL)  ");

            }
            //sql = string.Format("select * from View2 ");
            WriteToLog("Select:" + sql);
            return DataTableToJSONWithJSONNet(GetDataToDatatable(conn, sql));

        }


        public static string GetAlert(string MachineType, string PIC, string LineId)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            string sql = string.Empty;
            LineId = LineId.Replace('_', '.');
            sql = string.Format($"select * from View1 where MachineType='{MachineType}' and DataLineName='{LineId}' and Status='NEW' ");
            if (MachineType == "CPK")
            {
                if (PIC.Equals("admin"))
                    sql = string.Format($"select *,Cpk as ErrorCode from CPK where MachineType='{MachineType}'  and Status='ACK'");
                else
                    sql = string.Format($"select *,Cpk as ErrorCode from CPK where MachineType='{MachineType}'  and Status='ACK' AND PICName='{PIC}'  and  EquipmentId='{LineId}'  ");

            }
            else
            {
                if (PIC.Equals("admin"))
                    sql = string.Format($"select * from View1 where MachineType='{MachineType}' and LineName='{LineId}'  and Status='ACK'");
                else
                    sql = string.Format($"select * from View1 where MachineType='{MachineType}' and LineName='{LineId}'  and Status='ACK'  AND (onoff=1 or onoff is NULL) AND PICName='{PIC}' ");

                WriteToLog(sql);
            }

            // var sql = string.Format("select * from product");select * from [sandisk].[dbo].[View1] where Status='ACK'
            return DataTableToJSONWithJSONNet(GetDataToDatatable(conn, sql));

        }

        #endregion

        #region DEK route
        public static string GetAllDek()
        {
            SqlConnection conn = new SqlConnection(connectionString);
            string sql = string.Empty;
            sql = string.Format("select * from DekView where Status='NEW' and (onoff=1 or onoff is NULL)  ");
            //sql = string.Format("select * from View2 ");
            return DataTableToJSONWithJSONNet(GetDataToDatatable(conn, sql));
        }

        public static string GetDekById(string PIC)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            string sql = string.Empty;

            if (PIC.Equals("admin"))
                sql = string.Format("select * from DekView where Status='NEW'");
            else
                sql = string.Format("select * from DekView where Status='ACK'  AND (onoff=1 or onoff is NULL) AND PICName='{0}'", PIC);

            return DataTableToJSONWithJSONNet(GetDataToDatatable(conn, sql));
        }



        #endregion

        #region OtherRoute
        public static string CountAllMachine(AllMachineModel allMachine)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            string sql = " select  MachineType from data group by MachineType";
            DataTable MachineList = new DataTable();
            var dt1 = GetDataToDatatable(conn, sql);

            string LineList = "";

            var _linelist = allMachine.LineList.Split(',');
            foreach (var item in _linelist)
            {
                LineList = LineList + "'" + item + "',";
            }

            LineList = LineList.Remove(LineList.Length - 1);

            foreach (DataRow row in dt1.Rows)
            {
                var MachineType = row["MachineType"].ToString();
                if (MachineType != "CPK")
                {
                    sql = $"SELECT count(MachineType) as ErrorCount FROM View1 where Status in ('NEW','ACK') and LineName in ({LineList}) and onoff ='1' and MachineType='" + MachineType + "' ";
                }
                else
                {
                    sql = $"SELECT count(MachineType) as ErrorCount FROM View1 where Status in ('NEW','ACK')  AND MachineType='" + MachineType + "' ";
                }

                WriteToLog(sql);
                var tb1 = GetDataToDatatable(conn, sql);
                DataColumn workCol = tb1.Columns.Add("MachineType", typeof(String));
                tb1.Columns.Add("OtherParam", typeof(String));
                tb1.Rows[0]["MachineType"] = MachineType;
                tb1.Rows[0]["OtherParam"] = "None";

                MachineList.Merge(tb1);
            }



            return DataTableToJSONWithJSONNet(MachineList);
        }

        #endregion

        public static string ReturnAlert(string PIC)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            string sql = string.Empty;

            if (PIC.Equals("admin"))
                sql = string.Format("select * from View1 where Status='NEW'");
            else
                sql = string.Format("select * from View1 where Status='ACK'  AND (onoff=1 or onoff is NULL) AND PICName='{0}'", PIC);

            // var sql = string.Format("select * from product");select * from [sandisk].[dbo].[View1] where Status='ACK'
            return DataTableToJSONWithJSONNet(GetDataToDatatable(conn, sql));

        }

        public static string UpdateAlert(string alertId, string PIC)
        {
            string Stat = "";
            SqlConnection conn = new SqlConnection(connectionString);
            var date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            var sql = string.Format("UPDATE Data SET WaitingTime='{0}',PICName='{1}',Status='ACK' WHERE Id ='{2}' ", date, PIC, alertId);
            var pass = AddOrUpdateToDatabase(conn, sql);

            if (pass == false)
                Stat = "ERROR";
            else
                Stat = "AlertAck";

            return Stat;
        }

        public static void UpdateDoneAlert(string alertId, string PIC, string Message)
        {
            SqlConnection conn = new SqlConnection(connectionString);
            var date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            var sql = string.Format("UPDATE Data SET RepairTime='{0}',PICName='{1}',Status='DONE',ActionTaken='{3}' WHERE Id ='{2}' ", date, PIC, alertId, Message);
            AddOrUpdateToDatabase(conn, sql);
        }

        #region sql and json
        public static string Reset()
        {
            String stat = "";
            SqlConnection conn = new SqlConnection(connectionString);
            try
            {
                if (!dr.IsClosed)
                    dr.Close();

                if (conn.State == System.Data.ConnectionState.Open)
                    conn.Close();

                stat = "Success";
            }
            catch (Exception)
            {

                stat = "Error";
            }

            return stat;

        }

        public static string DataTableToJSONWithJSONNet(DataTable table)
        {
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(table);
            return JSONString;
        }




        public static bool AddOrUpdateToDatabase(SqlConnection conn, string sqlString)
        {
            bool isRemove = false;

            try
            {
                if (conn.State == System.Data.ConnectionState.Open)
                    conn.Close();

                conn.Open();
                SqlCommand com = new SqlCommand(sqlString, conn);
                com.ExecuteNonQuery();
                isRemove = true;


            }
            catch (SqlException e)
            {
                if (e.Message.ToString().Contains("duplicate"))
                {
                    isRemove = true;
                }
                else
                {
                    throw e;
                    if (isDomainName)
                    { conn.ConnectionString = "Data Source =sophicauto.ddns.net;User Id=azri;Password=azri;database=sandisk;"; isDomainName = false; }
                    else
                    { conn.ConnectionString = "Data Source =175.140.197.119;User Id=azri;Password=azri;database=sandisk;"; ; isDomainName = true; }
                }
                isRemove = false;
            }
            finally
            {
                conn.Close();
            }

            return isRemove;
        }

        public static string AddAndReturn(SqlConnection conn, string sqlString)
        {
            string result = "";

            try
            {
                conn.Open();
                SqlCommand com = new SqlCommand(sqlString, conn);
                //com.ExecuteNonQuery();
                result = com.ExecuteScalar().ToString();

            }
            catch (SqlException e)
            {
                result = "false:SQL" + e.Message;
            }
            finally
            {
                conn.Close();
            }
            return result;
        }

        public static string DataTableToJsonObj(DataTable dt)
        {
            DataSet ds = new DataSet();
            ds.Merge(dt);
            StringBuilder JsonString = new StringBuilder();
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                JsonString.Append("[");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    JsonString.Append("{");
                    for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
                    {
                        if (j < ds.Tables[0].Columns.Count - 1)
                        {
                            JsonString.Append("\"" + ds.Tables[0].Columns[j].ColumnName.ToString() + "\":" + "\"" + ds.Tables[0].Rows[i][j].ToString() + "\",");
                        }
                        else if (j == ds.Tables[0].Columns.Count - 1)
                        {
                            JsonString.Append("\"" + ds.Tables[0].Columns[j].ColumnName.ToString() + "\":" + "\"" + ds.Tables[0].Rows[i][j].ToString() + "\"");
                        }
                    }
                    if (i == ds.Tables[0].Rows.Count - 1)
                    {
                        JsonString.Append("}");
                    }
                    else
                    {
                        JsonString.Append("},");
                    }
                }
                JsonString.Append("]");
                return JsonString.ToString();
            }
            else
            {
                return null;
            }
        }

        public static DataTable GetDataToDatatable(SqlConnection conn, string query, string type = "")
        {
            lock (DataReadLock)
            {
                dt = new DataTable();

                //c.Open();
                try
                {
                    conn.Open();

                    SqlCommand mCmd = new SqlCommand(query, conn);
                    mCmd.ExecuteNonQuery();
                    dr = mCmd.ExecuteReader();
                    dt.Load(dr);

                }
                catch (SqlException ex)
                {
                    var tt = ex;
                    //MessageBox.Show(e.ToString());
                }
                finally
                {
                    if (!dr.IsClosed && dr != null)
                        dr.Close();

                    conn.Close();
                }



                if (type == "PM")
                {
                    return dt;
                }
                else if (type == "")
                {
                    return dt;
                }
                else if (type != "User")
                {
                    foreach (DataRow dr in dt.Rows) // search whole table
                    {
                        var MachineName = dr[11];

                        if (MachineName.ToString() == "CPK") // if id==2
                        {
                            dr["MachineName"] = dr["DataLineName"];
                            dr["LineName"] = dr["productName"];
                        }
                    }
                }
                return dt;
            }

        }

        public static string GetAData(SqlConnection conn, string sqlString, string columName) //function to return a value
        {
            lock (GetADataLock)
            {
                string value = "0";

                try
                {
                    conn.Open();
                    SqlCommand mCmd = new SqlCommand(sqlString, conn);

                    mCmd.ExecuteNonQuery();
                    dr = mCmd.ExecuteReader();

                    if (dr.Read())
                    {
                        if (dr[columName].ToString().Length > 0)
                            value = dr[columName].ToString();


                    }
                }
                catch (SqlException e)
                {

                }
                finally
                {
                    if (!dr.IsClosed)
                        dr.Close();

                    conn.Close();
                }

                return value;
            }

        }


        private static string Encrypt(string Password)
        {
            //create new instance of md5
            MD5 md5 = MD5.Create();

            //convert the input text to array of bytes
            byte[] hashData = md5.ComputeHash(Encoding.Default.GetBytes(Password));

            //create new instance of StringBuilder to save hashed data
            StringBuilder returnValue = new StringBuilder();

            //loop for each byte and add it to StringBuilder
            for (int i = 0; i < hashData.Length; i++)
            {
                returnValue.Append(hashData[i].ToString());
            }

            // return hexadecimal string
            string hashPassword = returnValue.ToString();

            return hashPassword;
        }

        #endregion

        private static void WriteToLog(string val)
        {
            if (DebugMode == "false")
            {
                return;
            }
            // Create a file to write to.
            string read = "";
            try
            {
                string path = AppDomain.CurrentDomain.BaseDirectory.ToString() + "Log/debug.txt";
                string createText = val + Environment.NewLine;
                File.AppendAllText(path, createText);
            }
            catch (Exception ex)
            {
                read = ex.Message;
            }
        }

        //reserved notused
        private DataTable objToDataTable(dynamic obj)
        {
            DataTable dt = new DataTable();
            //Mktdetails obj = new Mktdetails();
            //dt = objToDataTable(obj);

            //DataTable dt = new DataTable();
            //Mktdetails objmkt = new Mktdetails();
            //dt.Columns.Add("Column_Name");
            //foreach (PropertyInfo info in typeof(Mktdetails).GetProperties())
            //{
            //    dt.Rows.Add(info.Name);
            //}
            //dt.AcceptChanges();
            return dt;
        }

    }
}